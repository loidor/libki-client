Libki Kiosk Management System Client - the Swedish patch

Installationshänvisningar
*******************************

1. Installera mjukvaran som vanligt från https://bitbucket.org/libki-kms/libki-client/downloads/Libki_Client_2.0.2.0_(14.02_Leela)_Installer.exe
2. Kopiera in patchen i installationsmappen. (Patchen finns i https://bitbucket.org/loidor/libki-client/downloads/ 
3. Starta om systemet och ge patchen tillåtelse att starta utan prompt.

Funktionalitet:
*****************

Patchen lägger till svenska som språkalternativ i bokningssystemet. Språket bestäms utifrån systemets locale-inställningar.

Manual:
**********
Inställningar för klienten genereras vid installation och sparas i C:\Users\*USERNAME*\AppData\Libkiclient

Inställningar för datornamn, datorns plats, serverns adress samt port väljs vid installation,  men finns också i ovan nämnda ini-fil för ändring.
Vid installation väljs också upplåsningslösenord som nås via (shift-ctrl-alt-l). Det kan väljas om genom att köra create_password i C:\Program Files\Libki\windows. Upplåsningslösenord kan också läggas in i ini-filen i form av en md5-checksum. Det läggs i så fall in under själva installationen. För att ändra lösenordet i ini-filen behöver det göras om till en md5-checksum. Det görs förslagsvis på http://md5.cz. 

Inställningar för utloggningshändelse sätts också i ini-filen. Default är att användarkontot loggas ut tillsammans med att datorn stängs av, men det går också att bara gå tillbaka till Libkis inloggningsskärm eller att helt starta om maskinen.

Se example.ini i source för utförligare dokumentation på ini-filen.

För att inte äventyra säkerheten på lösenorden behöver följande mappar göras otillgängliga för slutanvändare:
1. C:\Program Files
2. C:\Users\*ANVÄNDARE*\AppData

Ändringar gjorda jämfört med originalet:
****************************************

1.  Klienten har fått en patch som översätter all text till svenska.
    Libkiklienten är skriven i QT Creator - en utvecklingsmiljö för C++ med fint stöd för översättning.
    1.1 Översättningen ligger i languages/libkiclient_sv_SE.ts och skriven i XML. (Se exempel nedan) 
    Delen av programmet det gäller anges i <name>, filen och raden det gäller anges i <location> och texten som ska översättas anges i <source>. 
    Den översatta texten anges avslutningsvis i <translation>. Om samma textrad ska översättas två gånger anger en två <location>-taggar.
        
        <?xml version="1.0" encoding="utf-8"?>
        <!DOCTYPE TS>
        <TS version="2.0" language="sv_SE">
            <context>
                <name>LoginWindow</name>
                <message>
                    <location filename="../loginwindow.ui" line="14"/>
                    <source>Libki Kiosk System</source>
                    <translation>Libki datorbokningssystem</translation>
                </message>
                <message>
                    <location filename="../loginwindow.ui" line="56"/>
                    <source>Internet Kiosk</source>
                    <translation>Publik dator</translation>
                </message>
                ...
            </context>
        </TS>
    1.2 languages/libkiclient_sv_SE.ts har sedan kompilerats till en *.qm-fil med QT Linguist och båda filerna har länkats in i Libki.pro (*.ts-filen på rad 23/24) samt i libki.qrc (*.qm-filen på rad 19).
    1.3 Hela rasket har sedan kompilerats om för att få en ny libkiclient.exe-fil med översättningarna i. 
        Översättningen görs utifrån vad maskinen har för locale-inställningar, så är det en maskin med inställningar för sverige är klienten på svenska, är det en maskin med franska körs klienten på franska, annars går den tillbaka till engelska.
        Dock är ingen ny installerare byggd, utan översättningen erbjuds i dagsläget som en patch.
    
2.  Inga andra ändringar är gjorda i klienten.